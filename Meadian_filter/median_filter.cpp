#include<iostream>

#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;
int dim;
void insertionSort(int window[])
{
    int temp, i , j;
    for(i = 0; i < dim ; i++){
        temp = window[i];
        for(j = i-1; j >= 0 && temp < window[j]; j--){
            window[j+1] = window[j];
        }
        window[j+1] = temp;
    }
}

void median_filter(Mat src,Mat dst,int kdim,int c)
{	
	int dim=kdim*kdim;
	int window[dim]; 
	int k=0;  
      
      
       // assignming the sliding window
        for(int y = kdim/2; y < src.rows - kdim/2; y++){
            for(int x = kdim/2; x < src.cols - kdim/2; x+=c){
                // Pick up window element
                k=0;
               // window[0] = src.at<uchar>(y - 1 ,x - 1);
                for(int i=-(kdim/2);i<=(kdim/2);i++)
		{
			for(int j=-(kdim/2);j<=(kdim/2);j++)
			{
				
				window[k++] = src.at<uchar>(y + i ,x + j);
				
			}
		}
          // sort the window to find median
                insertionSort(window);
          // assign the median to centered element of the matrix
                int mid=(dim/2)-1;
                dst.at<uchar>(y,x) = window[mid];
            }
        }
 
}



int main(int argc,char * argv[])
{
     
      Mat src, dst;
      // Load an image
      string image=argv[1];
      src = imread(image,IMREAD_GRAYSCALE);
      if( !src.data )
      {
       return -1; 
      }
      //create a sliding window of size 
      int kdim=atoi(argv[2]);
    
      dim=kdim*kdim;
      int window[dim]; 
      int k=0;  
      dst = src.clone();
      int striding_value=atoi(argv[3]);
      
      median_filter(src,dst,kdim, striding_value);
    
      
      
      
      
      
        namedWindow("final");
        imshow("final", dst);
        namedWindow("initial");
        imshow("initial", src);
         waitKey();
        
         
    return 0;
}
