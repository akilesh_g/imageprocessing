#include<iostream>

#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;

void median_filter_rgb(Mat src_rgb,Mat dst_rgb,int kdim,int striding_value)
{
    int dim=kdim*kdim;
    int imageChannels = dst_rgb.channels();
    vector<vector<int>> values (imageChannels);
    int m = kdim / 2;
    int pos = kdim * kdim / 2 ;
    for (int y = m; y < dst_rgb.rows - m; y++)
    {
        for (int x = m; x < dst_rgb.cols - m; x+=striding_value)
        {
            for (int channel = 0; channel < imageChannels; channel++)
            {
                values[channel].clear();
            }
            for (int i = -m; i <= m; i++)
            {
                for (int j = -m; j <= m; j++)
                {
                    unsigned char * pixelValuePtr;
                    for (int channel = 0; channel < imageChannels; channel++)
                    {
                        pixelValuePtr = src_rgb.ptr(y + j) + ((x + i) * imageChannels) + channel;
                        values[channel].push_back(*pixelValuePtr);
                    }
                }
            }
            unsigned char * pixelValuePtr1;
            for (int channel = 0; channel < imageChannels; channel++)
            {
                sort(begin(values[channel]), end(values[channel]));
                pixelValuePtr1 = dst_rgb.ptr(y) + (x * imageChannels) + channel;
                *pixelValuePtr1 = values[channel][pos];
            }
        }
    }
}

int main(int argc,char * argv[])
{
      int dim;
      Mat src, dst,src_rgb,dst_rgb;
      // Load an image
      string image=argv[1];
      src_rgb=imread(image,IMREAD_COLOR);
      if( !src_rgb.data )
      {
       return -1; 
      }
      //create a sliding window of size 
      int kdim=atoi(argv[2]);
     // cin>>kdim;
      dim=kdim*kdim;
      int window[dim]; 
      int k=0;  
      dst_rgb=src_rgb.clone();
      int striding_value=atoi(argv[3]);
     
      clock_t begin=clock();
      median_filter_rgb(src_rgb,dst_rgb,kdim,striding_value);
      clock_t end= clock();
         
      double time_spent =0.0;
      time_spent += (double)(end - begin)*1000 / CLOCKS_PER_SEC;
      //time_spent *=1000;
      cout<<time_spent<<endl;
      
      
    
      namedWindow("final");
      imshow("final", dst_rgb);
      namedWindow("initial");
      imshow("initial", src_rgb);
      waitKey();
    return 0;
}